import { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";
import TodoList from "./components/TodoList";

const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() {
  const [todos, setTodos] = useState([]);
  // Utilisation du hook useEffect pour appeler l'API au montage du composant
  useEffect(() => {
    // Fonction pour appeler l'API et mettre à jour le state
    const fetchTodos = async () => {
      try {
        const response = await axios.get(API_URL);
        setTodos(response.data);
      } catch (error) {
        console.error("Erreur lors de la récupération des tâches :", error);
      }
    };
    fetchTodos();
  }, []); 
  // Le tableau vide [] signifie que ce useEffect s'exécute une seule fois au montage
  return (
    <div className="App">
      <TodoList todos={todos} />
    </div>
  );
}
export default App;

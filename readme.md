# React - Todo List v2 : appeler une API


## Ressource(s)

Comment utiliser Axios avec React: Le Guide Complet
React - Utiliser un Hook d’effet
Axios - Installation
Grafikart - Le hook useEffect

## Contexte du projet

Dans ce brief, tu vas devoir compléter un projet qui a été démarré par une autre personne.

Commence par faire un fork du dépôt suivant : https://gitlab.com/adeline64/react-todo-list-v2-api

Tu ne sait pas ce qu’est un fork ? direction => https://git-scm.com/book/fr/v2/GitHub-Contribution-%C3%A0-un-projet#:~:text=Si%20vous%20souhaitez%20contribuer%20%C3%A0,et%20vous%20pouvez%20pousser%20dessus.

​Ensuite clone ton fork en local.

## Voici ce qui a été fait :

src/App.jsx: à modifier, contient actuellement un state vide todos, qui devra contenir la liste des objets chargés de l'API
src/TodoList.jsx: contient du code de l'affichage de la liste, il n'est pas à modifier
src/TodoItem.jsx: contient le code nécessaire à l'affichage d'une tâche, il n'est pas à modifier
​
Tu dois modifier le composant App afin de charger la liste des tâches à réaliser à partir d'une API dont le lien est le suivant (le lien est présent dans le code, dans la constante API_URL) : https://jsonplaceholder.typicode.com/todos

Pour charger l'API, tu devras utiliser la bibliothèque axios, qui tu devras installer dans le projet : https://axios-http.com/fr/docs/intro

Pour exécuter une action au montage d'un composant (la première fois qu'un composant est affiché), il faut utiliser le Hook useEffect : https://fr.reactjs.org/docs/hooks-effect.html

Pour charger une API, tu peux suivre l'explication de ce document (ou trouver tes propres ressources bien sûr) : Comment utiliser Axios avec React: Le Guide Complet

Une fois l'API chargée, tu stockera ses données dans le state todos.
